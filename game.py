def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board):
    print_board(board)
    print("GAME OVER")
    print(current_player, "has won")
    exit()

def is_row_winner(board, row_number):
    adjust = (row_number - 1) * 3
    return board[0 + adjust] == board[1 + adjust] and board[1 + adjust] == board[2 + adjust]

def is_column_winner(board, column_number):
    adjust = column_number - 1
    return board[0 + adjust] == board[3 + adjust] and board[3 + adjust] == board[6 + adjust]

def is_diagonal_winner(board, diagonal_number):
    adjust = (diagonal_number - 1) * 2
    return board[0 + adjust] == board[4] and board[4] == board[8 - adjust]


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board)
    elif is_row_winner(board, 2):
        game_over(board)
    elif is_row_winner(board, 3):
        game_over(board)
    elif is_column_winner(board, 1):
        game_over(board)
    elif is_column_winner(board, 2):
        game_over(board)
    elif is_column_winner(board, 3):
        game_over(board)
    elif is_diagonal_winner(board, 1):
        game_over(board)
    elif is_diagonal_winner(board, 2):
        game_over(board)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
